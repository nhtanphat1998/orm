﻿// khởi tạo đối tượng context đại diện cho 1 database
AppDbContext context = new AppDbContext();
List<string> names = context.Persons.Select(x => x.Name).ToList();

foreach (string name in names)
{
    Console.WriteLine(name);
}

List<string> nameAfter1992 = context.Persons.Where(x => x.DOB.Year > 1993 ).Select(x => x.Name).ToList();

foreach (string name in nameAfter1992)
{
    Console.WriteLine(name);
}

// làm tương tự với những từ khóa LinQ đã học và bật SQL profiler lên xem câu truy vấn được generate như thế nào
// orderby
// groupby
// aggregate function sum, avg, count, max, min
// first, firstOrDefault, last, LastOrDefault, Single, SingleOrDefault, paging,
