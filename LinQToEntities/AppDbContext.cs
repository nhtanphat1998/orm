﻿using Microsoft.EntityFrameworkCore;

public class AppDbContext : DbContext
{
    public DbSet<Person> Persons { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        string connectionString = "Data Source=LAPTOP-NR588SC6\\SQLEXPRESS;Database=LinQToEntities;Trusted_Connection=True;TrustServerCertificate=True";
        optionsBuilder.UseSqlServer(connectionString);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Person>().HasData(
            new Person { Id = 1, Name = "A", DOB = new DateTime(1990, 1, 1), Address = "1" },
            new Person { Id = 2, Name = "B", DOB = new DateTime(1991, 1, 1), Address = "2" },
            new Person { Id = 3, Name = "C", DOB = new DateTime(1992, 1, 1), Address = "3" },
            new Person { Id = 4, Name = "D", DOB = new DateTime(1993, 1, 1), Address = "4" },
            new Person { Id = 5, Name = "E", DOB = new DateTime(1994, 1, 1), Address = "5" },
            new Person { Id = 6, Name = "F", DOB = new DateTime(1995, 1, 1), Address = "6" },
            new Person { Id = 7, Name = "A", DOB = new DateTime(1996, 1, 1), Address = "7" }
        );
    }

}
