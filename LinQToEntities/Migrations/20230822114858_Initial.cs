﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace LinQToEntities.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DOB = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Persons",
                columns: new[] { "Id", "Address", "DOB", "Name" },
                values: new object[,]
                {
                    { 1, "1", new DateTime(1990, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "A" },
                    { 2, "2", new DateTime(1991, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "B" },
                    { 3, "3", new DateTime(1992, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "C" },
                    { 4, "4", new DateTime(1993, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "D" },
                    { 5, "5", new DateTime(1994, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "E" },
                    { 6, "6", new DateTime(1995, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "F" },
                    { 7, "7", new DateTime(1996, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "A" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Persons");
        }
    }
}
