﻿// tạo person sẽ thêm vào database
using Microsoft.EntityFrameworkCore.ChangeTracking;

Person person = new Person
{
    Name = "A",
};

// khởi tạo đối tượng context đại diện cho 1 database
AppDbContext context = new AppDbContext();

EntityEntry<Person> entity = context.Entry(person);

Console.WriteLine($"trước khi add {entity.State}"); 

// đánh dấu là thêm vào database
context.Persons.Add(person);
Console.WriteLine($"sau khi add {entity.State}");

// lưu mọi sự thay đổi
context.SaveChanges();
Console.WriteLine($"sau khi saveChanges {entity.State}");

// Chỉnh sửa chút
person.Name = "B";
Console.WriteLine($"hahaa {entity.State}");
