﻿AppDbContext dbContext = new AppDbContext();
// 1. Hãy liệt kê tất cả các quốc gia mà các sản phẩm trong cửa hàng được sản xuất.
List<string> distinctOrigins = dbContext.Products.Select(p => p.Origin).Distinct().ToList();

// 2. Liệt kê tất cả các "loại sản phẩm" duy nhất có trong cửa hàng.
List<string> distinctCategories = dbContext.Products.Select(p => p.Category).Distinct().ToList();