﻿using Microsoft.EntityFrameworkCore;

public class AppDbContext : DbContext
{
    public DbSet<Product> Products { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        string connectionString = "Data Source=DESKTOP-KF6RCOI;Initial Catalog=Linq2Entity;Persist Security Info=True;User ID=sa;Password=phat1234;Pooling=False;MultipleActiveResultSets=False;Encrypt=False;TrustServerCertificate=True";
        optionsBuilder.UseSqlServer(connectionString);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Product>().HasData(
            new Product { Id = 1, Name = "Laptop", Price = 1200, Quantity = 15, ManufactureDate = new DateTime(2022, 3, 10), IsActive = true, Category = "Electronics", Origin = "USA" },
            new Product { Id = 2, Name = "Smartphone", Price = 800, Quantity = 20, ManufactureDate = new DateTime(2022, 3, 12), IsActive = true, Category = "Electronics", Origin = "China" },
            new Product { Id = 3, Name = "Tablet", Price = 500, Quantity = 10, ManufactureDate = new DateTime(2022, 3, 14), IsActive = false, Category = "Electronics", Origin = "Korea" },
            new Product { Id = 4, Name = "Headphones", Price = 100, Quantity = 30, ManufactureDate = new DateTime(2022, 3, 16), IsActive = true, Category = "Electronics", Origin = "Japan" },
            new Product { Id = 5, Name = "Keyboard", Price = 50, Quantity = 25, ManufactureDate = new DateTime(2022, 3, 18), IsActive = false, Category = "Electronics", Origin = "Taiwan" },
            new Product { Id = 6, Name = "Mouse", Price = 30, Quantity = 40, ManufactureDate = new DateTime(2022, 3, 20), IsActive = true, Category = "Electronics", Origin = "China" },
            new Product { Id = 7, Name = "Monitor", Price = 300, Quantity = 5, ManufactureDate = new DateTime(2022, 3, 22), IsActive = true, Category = "Electronics", Origin = "Korea" },
            new Product { Id = 8, Name = "Printer", Price = 200, Quantity = 8, ManufactureDate = new DateTime(2022, 3, 24), IsActive = false, Category = "Electronics", Origin = "Japan" },
            new Product { Id = 9, Name = "Camera", Price = 400, Quantity = 12, ManufactureDate = new DateTime(2022, 3, 26), IsActive = true, Category = "Electronics", Origin = "China" },
            new Product { Id = 10, Name = "Speaker", Price = 150, Quantity = 18, ManufactureDate = new DateTime(2022, 3, 28), IsActive = true, Category = "Electronics", Origin = "USA" },
            new Product { Id = 11, Name = "External Hard Drive", Price = 80, Quantity = 22, ManufactureDate = new DateTime(2022, 3, 30), IsActive = true, Category = "Accessories", Origin = "Taiwan" },
            new Product { Id = 12, Name = "Router", Price = 120, Quantity = 17, ManufactureDate = new DateTime(2022, 4, 1), IsActive = false, Category = "Electronics", Origin = "China" },
            new Product { Id = 13, Name = "Smartwatch", Price = 200, Quantity = 13, ManufactureDate = new DateTime(2022, 4, 3), IsActive = true, Category = "Wearables", Origin = "Korea" },
            new Product { Id = 14, Name = "Printer Ink", Price = 40, Quantity = 28, ManufactureDate = new DateTime(2022, 4, 5), IsActive = true, Category = "Accessories", Origin = "USA" },
            new Product { Id = 15, Name = "Gaming Chair", Price = 250, Quantity = 9, ManufactureDate = new DateTime(2022, 4, 7), IsActive = false, Category = "Furniture", Origin = "China" },
            new Product { Id = 16, Name = "USB Flash Drive", Price = 20, Quantity = 35, ManufactureDate = new DateTime(2022, 4, 9), IsActive = true, Category = "Accessories", Origin = "Taiwan" },
            new Product { Id = 17, Name = "Wireless Earbuds", Price = 100, Quantity = 24, ManufactureDate = new DateTime(2022, 4, 11), IsActive = true, Category = "Accessories", Origin = "USA" },
            new Product { Id = 18, Name = "External Monitor", Price = 400, Quantity = 7, ManufactureDate = new DateTime(2022, 4, 13), IsActive = false, Category = "Electronics", Origin = "Japan" },
            new Product { Id = 19, Name = "Desk Lamp", Price = 30, Quantity = 19, ManufactureDate = new DateTime(2022, 4, 15), IsActive = true, Category = "Home", Origin = "China" },
            new Product { Id = 20, Name = "Power Bank", Price = 50, Quantity = 14, ManufactureDate = new DateTime(2022, 4, 17), IsActive = true, Category = "Accessories", Origin = "Taiwan" }

        );
    }

}


public class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public decimal Price { get; set; }
    public int Quantity { get; set; }
    public DateTime ManufactureDate { get; set; }
    public bool IsActive { get; set; }
    public string Category { get; set; }
    public string Origin { get; set; }
}