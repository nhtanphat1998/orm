﻿// khởi tạo đối tượng context đại diện cho 1 database
using Newtonsoft.Json;

AppDbContext context = new AppDbContext();

// tạo danh sách Product sẽ thêm vào database
StreamReader s = new StreamReader("MOCK_DATA.json");
JsonTextReader jr = new JsonTextReader(s);
JsonSerializer js = new JsonSerializer();
List<Product> obj = js.Deserialize<List<Product>>(jr);
// đánh dấu là thêm vào database
context.Products.AddRange(obj);

// lưu mọi sự thay đổi
context.SaveChanges();
int number = context.Products.Count();
Console.WriteLine($"number of record is: {number}");

//Lấy danh sách tất cả các sản phẩm.

//Lấy danh sách tên của tất cả các sản phẩm.

//Lấy danh sách các sản phẩm có giá lớn hơn 50 đồng.

//Lấy danh sách các sản phẩm có số lượng tồn kho ít hơn 10.

//Lấy danh sách tên của các sản phẩm có giá từ 100 đến 500 đồng.

//Lấy danh sách các sản phẩm được tạo ra trong vòng 7 ngày gần đây.

//Lấy danh sách các sản phẩm không còn tồn tại (không hoạt động).

//Lấy tổng số lượng tồn kho của tất cả sản phẩm.

//Lấy tên của sản phẩm và ngày tạo của sản phẩm có giá lớn hơn 200 đồng.

//Lấy danh sách các sản phẩm được sắp xếp theo giá giảm dần.


//Order By:

//Lấy danh sách các sản phẩm được sắp xếp theo tên sản phẩm tăng dần.
//Lấy danh sách các sản phẩm được sắp xếp theo giá giảm dần.
//Lấy danh sách các sản phẩm được sắp xếp theo ngày tạo mới nhất đến cũ nhất.


//Group By:

//Lấy danh sách các sản phẩm được nhóm theo số lượng tồn kho. (Mỗi nhóm hiển thị số lượng và tên các sản phẩm trong nhóm đó)
//Lấy danh sách các sản phẩm được nhóm theo khoảng giá (0-100, 101-200, v.v.). (Mỗi nhóm hiển thị khoảng giá và số lượng sản phẩm trong nhóm đó)

//First, FirstOrDefault, Last, LastOrDefault, Single, SingleOrDefault:

//Lấy thông tin chi tiết của sản phẩm đầu tiên trong danh sách.
//Lấy thông tin chi tiết của sản phẩm có giá cuối cùng.
//Lấy thông tin chi tiết của sản phẩm có tên là "iPhone" (nếu có).
//Lấy thông tin chi tiết của sản phẩm có ID là 5 (nếu có).
//Lấy thông tin chi tiết của sản phẩm có giá là 100 (nếu có).


//Paging:

//Lấy danh sách 10 sản phẩm đầu tiên trong danh sách.
//Lấy danh sách 20 sản phẩm từ vị trí thứ 21 trong danh sách.
